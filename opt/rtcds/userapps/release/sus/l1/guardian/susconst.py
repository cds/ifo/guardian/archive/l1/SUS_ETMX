# ramp time for output gains
default_ramp_time = 2.0
align_ramp_time = 5.0
isc_ramp_time = 5.0
rampTime = 5.0
coildriver_ramp_time = 8.0

use_olDamp = True
ol_off_threshold = 700
ol_on_threshold = 400

alignment_dofs = ['P', 'Y']

misalign_values = {'BS': {'P': 0.0, 'Y': 0.0},
                   'ETMX': {'P': 0.0, 'Y': 0.0},
                   'ETMY': {'P': 0.0, 'Y': 0.0},
                   'IM1': {'P': 0.0, 'Y': 0.0},
                   'IM2': {'P': 0.0, 'Y': 0.0},
                   'IM3': {'P': 0.0, 'Y': 0.0},
                   'IM4': {'P': 0.0, 'Y': 0.0},
                   'ITMX': {'P': 0.0, 'Y': 0.0},
                   'ITMY': {'P': 0.0, 'Y': 0.0},
                   'MC1': {'P': 0.0, 'Y': 0.0},
                   'MC2': {'P': 0.0, 'Y': 0.0},
                   'MC3': {'P': 0.0, 'Y': 0.0},
                   'OFI': {'P': 0.0, 'Y': 0.0},
                   'OM1': {'P': 0.0, 'Y': 0.0},
                   'OM2': {'P': 0.0, 'Y': 0.0},
                   'OM3': {'P': 0.0, 'Y': 0.0},
                   'OMC': {'P': 0.0, 'Y': 0.0},
                   'OPO': {'P': 0.0, 'Y': 0.0},
                   'PR2': {'P': 0.0, 'Y': 0.0},
                   'PR3': {'P': 0.0, 'Y': 0.0},
                   'PRM': {'P': 0.0, 'Y': 0.0},
                   'RM1': {'P': 0.0, 'Y': 0.0},
                   'RM2': {'P': 0.0, 'Y': 0.0},
                   'SR2': {'P': 0.0, 'Y': 0.0},
                   'SR3': {'P': 0.0, 'Y': 0.0},
                   'SRM': {'P': 0.0, 'Y': 0.0},
                   'TMSX': {'P': 0.0, 'Y': 0.0},
                   'TMSY': {'P': 0.0, 'Y': 0.0},
                   'FC1': {'P': 0.0, 'Y': 0.0},
                   'FC2': {'P': 0.0, 'Y': 0.0},
                   'ZM1': {'P': 0.0, 'Y': 0.0},
                   'ZM2': {'P': 0.0, 'Y': 0.0},
                   'ZM3': {'P': 0.0, 'Y': 0.0},
                   'ZM4': {'P': 0.0, 'Y': 0.0},
                   'ZM5': {'P': 0.0, 'Y': 0.0},
                   'ZM6': {'P': 0.0, 'Y': 0.0}
}

wd_threshold_high = 80000
wd_threshold_nominal = 25000

#Coil drive state definitions
quad_matrix = {'L1':{'l':0.25,'p':3.8462,'y':3.8462},'L2':{'l':0.25,'p':3.5355,'y':3.5355}}
bs_matrix = {'M2':{'l':0.25,'p':3.5361,'y':3.5361}}
hsts_matrix = {'M3':{'l':0.25,'p':5.2382,'y':5.2382}}

#Unlisted suspension don't have ramping matrices, so can't rotate off a given quadrant, so need for them here.
matrix_values = {'ETMX': quad_matrix,
                 'ETMY': quad_matrix,
                 'ITMX': quad_matrix,
                 'ITMY': quad_matrix,
                 'BS': bs_matrix,
                 'MC1': hsts_matrix,
                 'MC2': hsts_matrix,
                 'MC3': hsts_matrix,
                 'PRM': hsts_matrix,
                 'PR2': hsts_matrix,
                 'SRM': hsts_matrix,
                 'SR2': hsts_matrix,
}



#Initial state at DOWN we want suspension in
coilstate_lock_acq = {'BS': {'M2':2},
                  'ETMX': {'L1':2, 'L2':2,}, #From ISC_LOCK DOWN
                  'ETMY': {'L1':3, 'L2':2,}, #From ISC_LOCK DOWN
                  'ITMX': {'L1':3, 'L2':2,},
                  'ITMY': {'L1':3, 'L2':2,},
                  'IM1': {},
                  'IM2': {},
                  'IM3': {},
                  'IM4': {},
                  'MC1': {},
                  'MC2': {},
                  'MC3': {},
                  'OFI': {},
                  'OM1': {},
                  'OM2': {},
                  'OM3': {},
                  'OMC': {},
                  'OPO': {},
                  'PR2': {'M3':2},
                  'PR3': {},
                  'PRM': {'M3':2},
                  'RM1': {},
                  'RM2': {},
                  'SR2': {'M3':2},
                  'SR3': {},
                  'SRM': {'M3':2},
                  'TMSX': {},
                  'TMSY': {},
                  'ZM1': {},
                  'ZM2': {}
}

#First optional transition state      {'BS': {'M2':3},
coilstate_first_low_noise = {'BS': {'M2':[1,3]},
                  'ETMX': {'L2':3,},
                  'ETMY': {'L2':3,},
                  'ITMX': {'L2':3,},
                  'ITMY': {'L2':3,},
                  'MC1': {},
                  'MC2': {},
                  'MC3': {},
                  'PR2': {'M3':3},
                  'PR3': {},
                  'PRM': {'M3':3},
                  'RM1': {},
                  'RM2': {},
                  'SR2': {'M3':3},
                  'SR3': {},
                  'SRM': {'M3':3},
                  'TMSX': {},
                  'TMSY': {},
                  'ZM1': {},
                  'ZM2': {}
}

#Second optional transition state
coilstate_second_low_noise = {
                  'ETMX': {'L1':3,},
                  'ETMY': {'L1':3,},
}
